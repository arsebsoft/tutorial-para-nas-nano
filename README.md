# Utilización de la app NAS Nano
## Una guía tan sencilla de entender que hasta un gato podría usarla

## Instalación

Para instalar la app en tu dispositivo Android, [sigue este enlace](https://play.google.com/store/apps/details?id=io.nebulas.wallet.android). O puedes buscar la app en el Play Store directamente, con el nombre «NAS Nano Pro».

La instalación es igual a cualquier otra. Una vez finalizada, puedes pulsar en «Abrir aplicación».

### Solución a problemas frecuentes

#### Mi dispositivo no es compatible

¡Todavía puedes intentar una instalación directa!

![Recibir NAS](assets/2.jpg)

Primero tienes que habilitar la opción de instalar aplicaciones. Para ello debes ir a Ajustes (el icono de la rueda dentada), luego a «Pantalla Bloqueo y Seguridad» y allí debes buscar una opción que dice «Fuentes desconocidas». Haz clic en ella y, en el cartel de advertencia, clic en «Aceptar».

![Recibir NAS](assets/1.jpg)

Ahora necesitarás descargar [este APK](https://nano.nebulas.io/download/app/app-ch-MainNet-release.apk) en tu dispositivo Android. En cuanto se haya instalado, haz clic en la notificación, que te llevará a la ubicación del archivo .apk que has descargado.

![Recibir NAS](assets/3.jpg)

Haz clic en él. Acepta todas las advertencias. Cuando la app termine de instalarse, si todo salió bien y tu dispositivo no es totalmente incompatible, verás una notificación de que NAS Nano se ha instalado correctamente. ¡Ya puedes abrirla y utilizarla! Si ves que no hay caso, siempre puedes intentarlo en otros dispositivos, o utilizar el complemento de Chrome, [cuyo tutorial puedes hallar aquí](https://bitbucket.org/editorialarseb/nebulas-para-gatos/src/master/).

#### No puedo avanzar más allá de los términos y condiciones

Este es un problema común en algunos dispositivos muy pequeños que por lo general se marcan como _incompatibles_ (por lo que es muy probable que hayas instalado la app mediante el método anterior).

¡A no temer! La solución es muy sencilla. Sólo debes avanzar a través de todo el texto de los Términos y Condiciones. Cuando llegues al fondo, haz clic en donde dice «Acepto los términos...» y luego _intenta hacer clic debajo de la pantalla de tu dispositivo_. Esto puede llevarte unos segundos hasta que encuentras el lugar justo donde debería estar el botón que no ves en pantalla. Está centrado justo debajo de la pantalla.

Normalmente las pantallas táctiles tienen sensibilidad un poco por fuera de la pantalla física que ves en el teléfono o la tablet. Con un poco de suerte de tu lado, podrás pasar de esta pantalla y comenzar a usar la app.

## Uso básico

Una vez instalada la app, verás que tiene tres botones debajo: **Home**, **DApps** y **Me**. Arriba verás el saldo de tu cuenta (en dólares, por defecto), y un listado de novedades de Nebulas.

NAS Nano te permite enviar y recibir NAS, que es el uso básico. También te permite descubrir aplicaciones descentralizadas (DApps) como utilitarios y juegos. Adicionalmente te permite importar carteras creadas con otras herramientas como la cartera web, mediante la clave privada.

Pero veamos antes lo esencial.

### Recibir NAS

![Pantalla inicial](assets/5.jpg)

Inmediatamente después de abrir por primera vez tu cartera NAS Nano, verás que tu saldo está en cero. Pues bien, si alguien está dispuesto a enviarte algunos NAS, puedes solicitárselos mediante esta opción.

![Recibir NAS](assets/8.jpg)

En la pantalla inicial verás dos botones en la parte superior: «Receive» y «Send». Pulsa en _Receive_ y verás un código QR y una cadena de texto que comienza con **n1...**. Si tocas en el recuadrito con una flechita que apunta hacia arriba a la derecha (justo a la derecha del círculo y arriba a la derecha del código QR), verás que puedes compartir el código QR en las distintas apps instaladas en tu dispositivo. Por ejemplo, si quieres enviarle la dirección de tu cartera a un amigo a través de Whatsapp, no tienes más que pulsar esa flechita, elegir Whatsapp y luego elegir los mensajes con tu amigo. Lo mismo vale para las otras redes sociales.

### Enviar NAS

Para enviar NAS a otra persona, primero necesitas la dirección de su cartera. Cuando la recibas, ve a la pantalla de inicio (botón «Home» de la barra inferior en la app) y pulsa «Send». Verás una pantalla como la que pongo a continuación:

![Recibir NAS](assets/7.jpg)

En «Address» debes pegar la dirección que te han enviado. Procura copiarla (pulsa sobre ese texto en la app donde te la enviaron, espera unos segundos con el dedo presionado sobre ella, y verás que aparece una opción que dice «Copiar»). En este campo (Address), haz lo mismo: pulsa, mantén el dedo unos dos o tres segundos, y verás que aparece la opción «Pegar». Si haz hecho todo bien, verás allí la dirección a la cual quieres enviar tus NAS.

Abajo verás que dice «NAS», junto al logotipo de Nebulas. Eso déjalo así.

Más abajo debes especificar cuántos NAS estarás enviando. En mi caso, elegí enviar 4.

En «Memo» puedes escribir un breve texto recordatorio, para saber más adelante por qué has enviado esos NAS (algo útil para gente con poca memoria y con tendencia a confundirse, como yo :3).

Más abajo dice «Gas Fee». Esto es un parámetro más avanzado, que le indica al sistema, para hacerlo fácil de entender aquí, con cuánta prisa queremos enviar los NAS. Normalmente deberías dejar ese parámetro tal como está, pero si quieres experimentar no perderás nada. Al pulsar allí verás que aparece una barra deslizadora con varias opciones predefinidas. Mientras más a la derecha corras el deslizador, más NAS te descontará en concepto de _comisiones_ (aunque en cualquier caso será una cantidad absolutamente ínfima; la opción más cara representa apenas unas milésimas de centavo de dólar), que es lo que estás dispuesto a pagarles a quienes mantienen segura la red Nebulas. Mientras más desees pagarles, más rápído se procesará tu transacción. En mi caso, siempre elijo el máximo valor posible para el gas, ya que de esa forma contribuyo a que los _contadores_ de Nebulas reciban una recompensa justa por su trabajo, y de esa forma la red se mantiene segura.

Para confirmar la operación, pulsa en «Confirm». Aparecerá una pantalla adicional con la confirmación final. Revisa si está todo bien, y acepta la transacción. ¡Listo!

## Algunas DApps útiles

Además de poder enviar y recibir NAS, con NAS Nano podemos hacer uso de las aplicaciones descentralizadas que se encuentran disponibles en la red Nebulas.

Voy a hacer un pequeño repaso por las que a mi criterio son más útiles o interesantes para el usuario promedio. Estoy seguro de que encontrarás muchas que te gustarán, sobre todo los juegos.

### Custom NAS Wallet Aliases

Como su nombre lo indica, esta DApp nos permite crear un _alias_ o _pseudónimo_ para la dirección de nuestra cartera NAS. En vez de tener que intentar recordar todo eso de “n1...”, esta simpática aplicación nos permite obtener algo mucho más fácil de recordar, como una url que además permite compartirla en redes sociales.

![Nas.me](assets/9.jpg)

Para usarla sólo hace falta pulsar en el listado, y luego en el pequeño botón que dice «Play». Esto nos llevará a un [sitio web](https://nas.me/) que nos permite comprar (por 0,1 NAS) una URL del tipo `https://arseb.nas.me`

![Nas.me](assets/10.jpg)

El proceso es muy sencillo, aunque necesitarás contar con un navegador preparado para web 3.0.

### Neby

![Nebbot](assets/11.jpg)

[Neby es un bot](https://nebulearn.com/apps/neby/) que nos permite enviar y recibir NAS a través de la red social Twitter. En este caso no hace falta usar la app NAS Nano para interactuar con él.

Para usarlo, tienes que responder a un tweet de la persona a la cual quieres enviarle NAS, escribiendo lo siguiente:

```Tweet
@NebBot gift X NAS to NNNNN
```

En donde `X` es la cantidad de NAS que quieres enviar, y `NNNNN` es el nombre de la persona, que puede ser su @usuario o simplemente un apodo.

Es muy importante que sigas la cuenta [@nebbot](https://twitter.com/NebBot/) en Twitter, y que, para asegurarte de que todo funcione bien, le envíes un mensaje directo antes, de modo que pueda enviarte por allí lo necesario para hacer el envío de tus NAS.

Por supuesto, asegúrate primero de que tu amigo tiene cuenta en Nebulas. Si no la tiene, invítalo a abrir una cartera por cualquiera de los métodos que se discuten [en este otro tutorial](https://bitbucket.org/editorialarseb/nebulas-para-gatos).